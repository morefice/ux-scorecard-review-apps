## UX Scorecard Review Apps

This project is to be used to complete the tasks and scenario set out in the UX Scorecard - https://gitlab.com/gitlab-org/ux-research/-/issues/1630.

### Scenario
You are a frontend developer who is responsible for developing and testing new features, and successfully pushing them to production. To enable yourself and teammates to easily review new UI features, you need an environment to see and review the new changes, before it goes to production. This should help reduce any bugs and undesired or unexpected behavior before users interact with it.

### Tasks to complete the scenario:
1. Set up a review app for your project by configuring an environment to deploy changes to
1. Create an MR with a simple UI update on the home page that triggers the review app
1. Review the update and make any changes necessary to obtain the expected features
1. Merge the MR
